# Changelog

All notable changes to `profiling-laravel` will be documented in this file.

## 0.0.1 - 2024-04-01

- Experimental release

## 1.0.0 - 2024-04-29

- First version release


## 1.0.1 - 2024-04-30

- Change speed profiling global action detect


## 1.0.2 - 2024-09-09

- Add latency config set for `TimingsProfiling`.
- Add Introduction and How it works sections to README.md.
- Described `checkpoint` method of `SpeedProfiler` in README.md.
- Add an examples for every using point in README.md.
- Add total time spent on action to the profiling statistic.

