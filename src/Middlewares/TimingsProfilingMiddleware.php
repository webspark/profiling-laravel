<?php

declare(strict_types=1);

namespace Webspark\Profiling\Laravel\Middlewares;

use Webspark\Profiling\Laravel\Facades\TimingsProfiling;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Closure;

class TimingsProfilingMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (config('profiling.timings.enabled', true) === false) {
            return $next($request);
        }

        TimingsProfiling::init(defined('LARAVEL_START') ? LARAVEL_START : null);

        /** @var Response $response */
        $response = $next($request);

        [$headerName, $headerValue] = explode(': ', TimingsProfiling::header());
        $response->headers->set($headerName, $headerValue);

        return $response;
    }
}
