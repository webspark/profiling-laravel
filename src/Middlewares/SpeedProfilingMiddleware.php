<?php

declare(strict_types=1);

namespace Webspark\Profiling\Laravel\Middlewares;

use Webspark\Profiling\Profilers\SpeedProfiler;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Closure;

class SpeedProfilingMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (config('profiling.speed.enabled', true) === false) {
            return $next($request);
        }

        $actionName = Route::current()->getAction()['controller'] ?? null;

        if (empty($actionName)) {
            $actionName = Route::current()->getName();
        }

        $profiler = new SpeedProfiler($actionName ?? 'Unknown Route');

        return $next($request);
    }
}
