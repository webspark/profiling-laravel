<?php

declare(strict_types=1);

namespace Webspark\Profiling\Laravel\Commands;

use Webspark\Profiling\Services\SpeedProfilerStatisticService;
use Webspark\Profiling\Dto\ProfilerStatisticFilters;
use Webspark\Profiling\Enums\StatisticSortingType;
use Illuminate\Console\Command;

class SpeedProfilingStatistic extends Command
{
    /** @var string */
    protected $signature = 'profiling:speed-statistic {--date=} {--sort=} {--limit=}';

    /** @var string */
    protected $description = 'Generate speed profiling statistic. Arguments: --date - log date,
        --sort - sort by (max, avg, med, min, total, calls), --limit - count first rows.';

    public function handle(): int
    {
        $sort = $this->option('sort') ?: null;
        $filters = new ProfilerStatisticFilters(
            $this->option('limit') ?? 10,
            $this->option('date'),
            $sort ? new StatisticSortingType($sort) : null,
        );

        $statisticList = (new SpeedProfilerStatisticService())->generate($filters);

        foreach ($statisticList as $action => $statistic) {
            $this->output->text(sprintf(
                "Max: %7.2f \t| Avg: %7.2f \t| Med: %7.2f \t| Min: %7.2f \t| Total: %7.2f \t| Calls: %4d - %s",
                $statistic['max'],
                $statistic['avg'],
                $statistic['med'],
                $statistic['min'],
                $statistic['total'],
                $statistic['calls'],
                $action
            ));
        }

        $this->output->text('Sorted by ' . $sort . PHP_EOL);

        return 0;
    }
}
