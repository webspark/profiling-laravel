<?php

declare(strict_types=1);

namespace Webspark\Profiling\Laravel;

use Webspark\Profiling\Laravel\Commands\SpeedProfilingStatistic;
use Webspark\Profiling\Processors\InMemoryProfilingProcessor;
use Webspark\Profiling\Processors\ProfilingProcessorInterface;
use Webspark\Profiling\Providers\TimingsProfilingProvider;
use Webspark\Profiling\Providers\SpeedProfilingProvider;
use Webspark\Profiling\Laravel\Facades\TimingsProfiling;
use Webspark\Profiling\Laravel\Facades\SpeedProfiling;
use Illuminate\Database\Events\QueryExecuted;
use Webspark\Profiling\ProfilingConfig;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class ProfilingServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/config/profiling.php' => config_path('profiling.php'),
        ]);
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/profiling.php', 'profiling');

        $this->app->singleton(TimingsProfilingProvider::class, static function (): TimingsProfilingProvider {
            return TimingsProfilingProvider::getInstance();
        });

        $this->app->singleton(SpeedProfilingProvider::class, static function (): SpeedProfilingProvider {
            return SpeedProfilingProvider::getInstance();
        });

        $this->registerCommands();

        $this->registerTimings();
        $this->registerSpeed();
    }

    protected function registerTimings(): void
    {
        if ($this->app['config']->get('profiling.timings.enabled') === false) {
            return;
        }

        TimingsProfiling::setConfig(new ProfilingConfig([
            'latency' => config('profiling.timings.latency'),
        ]));

        // @codeCoverageIgnoreStart

        DB::listen(static function (QueryExecuted $query): void {
            TimingsProfiling::increment('Database', $query->time);
        });

        // @codeCoverageIgnoreEnd
    }

    protected function registerSpeed(): void
    {
        if ($this->app['config']->get('profiling.speed.enabled') === false) {
            return;
        }

        $this->commands(['profiling:speed-statistic']);

        SpeedProfiling::setProcessor($this->speedProfilingProcessor())
            ->setConfig(new ProfilingConfig(['latency' => config('profiling.speed.latency')]));
    }

    protected function speedProfilingProcessor(): ProfilingProcessorInterface
    {
        return new InMemoryProfilingProcessor();
    }

    protected function registerCommands(): void
    {
        $this->app->bind('profiling:speed-statistic', SpeedProfilingStatistic::class);
    }
}
