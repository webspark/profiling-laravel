<?php

return [
    'speed' => [
        'enabled' => env('PROFILING_SPEED_ENABLED', false),
        'latency' => env('PROFILING_SPEED_LATENCY', 1000), // in milliseconds, equal to 1 second
    ],

    'timings' => [
        'enabled' => env('PROFILING_TIMINGS_ENABLED', false),
        'latency' => env('PROFILING_TIMINGS_LATENCY', 0), // in milliseconds, by default all actions are recorded
    ],
];
