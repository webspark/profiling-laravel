<?php

declare(strict_types=1);

namespace Webspark\Profiling\Laravel\Processors;

use Webspark\Profiling\Processors\ProfilingProcessorInterface;
use Webspark\Profiling\Dto\ProfilingRow;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use SplFileObject;

class LogChannelProfilingProcessor implements ProfilingProcessorInterface
{
    protected LoggerInterface $logger;
    protected string $lineStartFrom = 'Profiling';
    protected array $loggerChannelConfig;

    public function __construct(string $channelName)
    {
        $this->loggerChannelConfig = config('logging.channels.' . $channelName);
        $this->logger = Log::channel($channelName);
    }

    public function write(ProfilingRow $profilingRow): void
    {
        $this->logger->info('Profiling', [
            'action' => $profilingRow->action,
            'timestamp' => $profilingRow->timestamp,
            'context' => $profilingRow->context,
        ]);
    }

    protected function logFiles(): array
    {
        $logPath = $this->loggerChannelConfig['path'];
        $pattern = str_replace('.log', '*.log', $logPath);

        return glob($pattern);
    }

    public function rows(): array
    {
        $logFiles = $this->logFiles();
        $rows = [];

        foreach ($logFiles as $logFile) {
            // @codeCoverageIgnoreStart

            if (is_file($logFile) === false) {
                continue;
            }

            // @codeCoverageIgnoreEnd

            $file = new SplFileObject($logFile);

            while (!$file->eof()) {
                $line = trim($file->getCurrentLine());

                if (empty($line) || strpos($line, $this->lineStartFrom) === false) {
                    continue;
                }

                $content = preg_replace('/\[[^]]+] .*(?=' . $this->lineStartFrom . ')/', '', $line);
                $content = str_replace($this->lineStartFrom, '', $content);
                $content = json_decode($content, true);

                $rows[] = new ProfilingRow(
                    (string) $content['action'],
                    (int) $content['timestamp'],
                    $content['context'],
                );
            }
        }

        return $rows;
    }

    public function clear(): void
    {
        $logFiles = $this->logFiles();

        foreach ($logFiles as $logFile) {
            if (is_writable($logFile)) {
                // @codeCoverageIgnoreStart
                // suppress errors here as unlink() might fail if two processes
                // are cleaning up/rotating at the same time
                set_error_handler(function (): bool {
                    return false;
                });

                unlink($logFile);
                restore_error_handler();
                // @codeCoverageIgnoreEnd
            }
        }
    }
}