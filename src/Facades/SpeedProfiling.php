<?php

declare(strict_types=1);

namespace Webspark\Profiling\Laravel\Facades;

use Webspark\Profiling\Processors\ProfilingProcessorInterface;
use Webspark\Profiling\Providers\SpeedProfilingProvider;
use Webspark\Profiling\ProfilingConfig;
use Illuminate\Support\Facades\Facade;

/**
 * @method static ProfilingProcessorInterface getProcessor() Get profiling processor.
 * @method static SpeedProfilingProvider setProcessor(ProfilingProcessorInterface $processor) Set profiling processor.
 * @method static ProfilingConfig getConfig() Get profiling config.
 * @method static SpeedProfilingProvider setConfig(ProfilingConfig $config) Set profiling config.
 */
class SpeedProfiling extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return SpeedProfilingProvider::class;
    }
}
