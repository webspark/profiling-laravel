<?php

declare(strict_types=1);

namespace Webspark\Profiling\Laravel\Facades;

use Webspark\Profiling\Providers\TimingsProfilingProvider;
use Webspark\Profiling\ProfilingConfig;
use Illuminate\Support\Facades\Facade;

/**
 * @method static void start(string $timingName) Start a timing event.
 * @method static void stop(string $timingName) End a timing event.
 * @method static void stopAllUnfinishedEvents() End an unfinished timing events.
 * @method static static setDuration(string $timingName, callable|float $duration) Set timing event duration.
 * @method static float|null getDuration(string $timingName) Get timing event duration.
 * @method static void increment(string $timingName, float $microTime) Increment a timing event on some time.
 * @method static array timings() Get finished timings.
 * @method static void init(?float $startTime = null) Init server timings.
 * @method static string header() Get header for server timings.
 * @method static ProfilingConfig getConfig() Get profiling config.
 * @method static TimingsProfiling setConfig(ProfilingConfig $config) Set profiling config.
 */
class TimingsProfiling extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return TimingsProfilingProvider::class;
    }
}