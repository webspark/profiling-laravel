<?php

declare(strict_types=1);

use Webspark\Profiling\Laravel\Middlewares\TimingsProfilingMiddleware;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

it('nothing to do if timings profiling not enabled', function () {
    Config::set('profiling.timings.enabled', false);

    $request = new Request;
    $middleware = new TimingsProfilingMiddleware();

    $response = $middleware->handle($request, static function () {
        return new Response();
    });

    expect(empty($response->headers->get('server-timing')))->toBeTrue();

    Config::set('profiling.timings.enabled', true);
});

it('can add server timing header', function () {
    $request = new Request;
    $middleware = new TimingsProfilingMiddleware();

    $response = $middleware->handle($request, static function () {
        return new Response();
    });

    expect(empty($response->headers->get('server-timing')))->toBeFalse();
});
