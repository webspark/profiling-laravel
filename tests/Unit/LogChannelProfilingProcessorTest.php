<?php

namespace Webspark\Profiling\Laravel\Tests\Unit;

use Webspark\Profiling\Laravel\Processors\LogChannelProfilingProcessor;
use Webspark\Profiling\Services\SpeedProfilerStatisticService;
use Webspark\Profiling\Laravel\Facades\SpeedProfiling;
use Webspark\Profiling\Dto\ProfilerStatisticFilters;
use Webspark\Profiling\Enums\StatisticSortingType;
use Webspark\Profiling\Dto\ProfilingRow;

it('can write and read logs', function () {
    $processor = new LogChannelProfilingProcessor('single');
    $processor->clear();

    $actions = [
        'testing-first-' . time(),
        'testing-second-' . time(),
        'testing-third-' . time(),
    ];

    foreach ($actions as $action) {
        $processor->write(new ProfilingRow($action, time(), ['exec-time' => 2000]));
    }

    SpeedProfiling::setProcessor($processor);

    foreach ($actions as $action) {
        expect(speedProfilingFindRow($action))->toBeInstanceOf(ProfilingRow::class);
    }
});

it('can calculate statistic correctly', function () {
    $processor = new LogChannelProfilingProcessor('single');
    $processor->clear();

    $execTimes = [2.0, 4.0, 3.0, 5.0, 4.0, 1.0, 9.0, 6.0, 2.0, 4.0];
    $action = 'testing-statistic-' . time();

    foreach ($execTimes as $execTime) {
        $processor->write(new ProfilingRow($action, time(), ['exec-time' => $execTime]));
    }

    SpeedProfiling::setProcessor($processor);

    $filters = new ProfilerStatisticFilters(20, date('Y-m-d'), new StatisticSortingType(StatisticSortingType::AVG));
    $statisticList = (new SpeedProfilerStatisticService())->generate($filters);

    expect(empty($statisticList[$action]))->toBeFalse()
        ->and(isset($statisticList[$action]['action']))->toBeTrue()
        ->and(isset($statisticList[$action]['max']))->toBeTrue()
        ->and($statisticList[$action]['max'] === 9.00)->toBeTrue()
        ->and(isset($statisticList[$action]['avg']))->toBeTrue()
        ->and($statisticList[$action]['avg'] === 4.0)->toBeTrue()
        ->and(isset($statisticList[$action]['med']))->toBeTrue()
        ->and($statisticList[$action]['med'] === 4.25)->toBeTrue()
        ->and(isset($statisticList[$action]['min']))->toBeTrue()
        ->and($statisticList[$action]['min'] === 1.00)->toBeTrue()
        ->and(isset($statisticList[$action]['total']))->toBeTrue()
        ->and($statisticList[$action]['total'] === 40.0)->toBeTrue()
        ->and(isset($statisticList[$action]['calls']))->toBeTrue()
        ->and($statisticList[$action]['calls'] === 10)->toBeTrue();
});

it('can calculate statistic by yesterday correctly', function () {
    $processor = new LogChannelProfilingProcessor('single');
    $processor->clear();

    $execTimes = [2.0, 4.0, 5.0, 4.0, 1.0, 9.0, 2.0, 4.0, 4.0, 2.0, 7.0];
    $timestamp = strtotime('-1 day');
    $action = 'testing-statistic-' . $timestamp;

    foreach ($execTimes as $execTime) {
        $processor->write(new ProfilingRow($action, $timestamp, ['exec-time' => $execTime]));
    }

    SpeedProfiling::setProcessor($processor);

    $filters = new ProfilerStatisticFilters(20, date('Y-m-d', $timestamp), new StatisticSortingType(StatisticSortingType::AVG));
    $statisticList = (new SpeedProfilerStatisticService())->generate($filters);

    expect(empty($statisticList[$action]))->toBeFalse()
        ->and(isset($statisticList[$action]['action']))->toBeTrue()
        ->and(isset($statisticList[$action]['max']))->toBeTrue()
        ->and($statisticList[$action]['max'] === 9.00)->toBeTrue()
        ->and(isset($statisticList[$action]['avg']))->toBeTrue()
        ->and($statisticList[$action]['avg'] === 4.0)->toBeTrue()
        ->and(isset($statisticList[$action]['med']))->toBeTrue()
        ->and($statisticList[$action]['med'] === 3.89)->toBeTrue()
        ->and(isset($statisticList[$action]['min']))->toBeTrue()
        ->and($statisticList[$action]['min'] === 1.00)->toBeTrue()
        ->and(isset($statisticList[$action]['total']))->toBeTrue()
        ->and($statisticList[$action]['total'] === 44.0)->toBeTrue()
        ->and(isset($statisticList[$action]['calls']))->toBeTrue()
        ->and($statisticList[$action]['calls'] === 11)->toBeTrue();
});
