<?php

declare(strict_types=1);

use Webspark\Profiling\Laravel\ProfilingServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

it('can be constructed', function () {
    expect(new ProfilingServiceProvider(app()))->toBeInstanceOf(ServiceProvider::class);
});

it('can be constructed with disabled profiling', function () {
    Config::set('profiling.speed.enabled', false);
    Config::set('profiling.timings.enabled', false);

    $serviceProvider = new ProfilingServiceProvider(app());
    $serviceProvider->boot();
    $serviceProvider->register();

    expect($serviceProvider)->toBeInstanceOf(ServiceProvider::class);

    Config::set('profiling.speed.enabled', true);
    Config::set('profiling.timings.enabled', true);
});
