<?php

declare(strict_types=1);

use Webspark\Profiling\Processors\InMemoryProfilingProcessor;
use Webspark\Profiling\Laravel\Facades\SpeedProfiling;
use Webspark\Profiling\Enums\StatisticSortingType;
use Webspark\Profiling\Dto\ProfilingRow;

test('command output test', function () {
    $processor = new InMemoryProfilingProcessor();
    $actions = [
        ['name' => 'testing-statistic-first', 'calls' => 10, 'avg' => 5.5, 'med' => 5.5, 'total' => 55.0],
        ['name' => 'testing-statistic-second', 'calls' => 20, 'avg' => 10.5, 'med' => 10.5, 'total' => 210.0],
        ['name' => 'testing-statistic-third', 'calls' => 10, 'avg' => 5.5, 'med' => 5.5, 'total' => 55.0],
    ];

    foreach ($actions as $action) {
        $execTimeMax = $action['calls'];

        while ($execTimeMax > 0) {
            $processor->write(new ProfilingRow($action['name'], time(), ['exec-time' => $execTimeMax]));
            $execTimeMax--;
        }
    }

    SpeedProfiling::setProcessor($processor);
    $artisan = $this->artisan('profiling:speed-statistic', ['--sort' => StatisticSortingType::AVG]);

    foreach ($actions as $action) {
        $artisan->expectsOutputToContain(sprintf(
            "Max: %7.2f \t| Avg: %7.2f \t| Med: %7.2f \t| Min: %7.2f \t| Total: %7.2f \t| Calls: %4d - %s",
            $action['calls'], $action['avg'], $action['med'], 1, $action['total'], $action['calls'], $action['name']
        ));
    }

    $artisan->expectsOutputToContain('Sorted by avg')
        ->assertExitCode(0);
});
