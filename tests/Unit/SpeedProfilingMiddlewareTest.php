<?php

declare(strict_types=1);

use Webspark\Profiling\Laravel\Middlewares\SpeedProfilingMiddleware;
use Illuminate\Support\Facades\Route as RouteFacade;
use Webspark\Profiling\Dto\ProfilingRow;
use Illuminate\Support\Facades\Config;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

it('nothing to do if speed profiling not enabled', function () {
    Config::set('profiling.speed.enabled', false);

    $route = new Route('GET', 'testing/nothing', []);
    $route->setAction(['controller' => 'TestingSpeedNothing']);

    RouteFacade::shouldReceive('current')->andReturn($route);

    $request = new Request;
    $middleware = new SpeedProfilingMiddleware();

    $middleware->handle($request, static function () {
        usleep(2000);

        return new Response();
    });

    expect(speedProfilingFindRow('TestingSpeedNothing'))->toBeNull();

    Config::set('profiling.speed.enabled', true);
});

it('can add speed log', function () {
    $route = new Route('GET', 'testing', []);
    RouteFacade::shouldReceive('current')->andReturn($route);

    $request = new Request;
    $middleware = new SpeedProfilingMiddleware();

    $middleware->handle($request, static function () {
        usleep(2000);

        return new Response();
    });

    expect(speedProfilingFindRow('Unknown Route'))->toBeInstanceOf(ProfilingRow::class);
});


