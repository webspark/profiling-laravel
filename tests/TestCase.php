<?php

namespace Webspark\Profiling\Laravel\Tests;

use Webspark\Profiling\Laravel\ProfilingServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function getPackageProviders($app): array
    {
        return [
            ProfilingServiceProvider::class,
        ];
    }
}
